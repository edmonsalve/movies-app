import React, { Component } from 'react';
import { MoviesList } from "./components/movie-list/movie-list.component";
import { Header } from "./components/header/header.component";

import { API_BASE_URL, API_TOP_MOVIES_URL, API_POPULAR_MOVIES_URL, API_KEY } from "./utils/constants";

import './App.css';
import { MovieDetail } from './components/movie-detail/movie-detail.component';

class App extends Component {

  constructor(props) {
    super();
    this.state = {
      topMovies: [],
      popularMovies: [],
      isOnDetailView: false,
      ratingFilter: 0
    }


    this.currentFilteredText = "";
    this.topMoviesCopy = [];
    this.popularMoviesCopy = [];
    this.selectedMovie = null;

    this.goToMovieDetail = this.goToMovieDetail.bind(this);
    this.activateRatingFilter = this.activateRatingFilter.bind(this);
    this.showMainPage = this.showMainPage.bind(this);
  }

  componentDidMount() {
    const topMoviesUrl = `${API_BASE_URL}${API_TOP_MOVIES_URL}?api_key=${API_KEY}`;
    fetch(topMoviesUrl)
      .then(response => response.json()
      .then(movies => {
        this.topMoviesCopy = movies.results;
        this.setState({ topMovies: movies.results })
      }));
    
    const popularMoviesUrl = `${API_BASE_URL}${API_POPULAR_MOVIES_URL}?api_key=${API_KEY}`;
    fetch(popularMoviesUrl)
      .then(response => response.json())
      .then(movies => {
        this.popularMoviesCopy = movies.results;
        this.setState({ popularMovies: movies.results })
      })
  }

  filterMovies(text) {
    if (text === "") {
      this.setState({ topMovies: this.topMoviesCopy, popularMovies: this.popularMoviesCopy });
      this.currentFilteredText = "";
      return;
    }

    if (this.state.ratingFilter !== 0) {
      this.filterByNameAndRating(text, this.state.ratingFilter);
    } else {
      this.filterByName(text);
    }

    this.currentFilteredText = text;
    
  }

  filterByName(text) {
    const topMovies = this.topMoviesCopy.filter(movie => movie.title.toLowerCase().includes(text.toLowerCase()));
    const popularMovies = this.popularMoviesCopy.filter(movie => movie.title.toLowerCase().includes(text.toLowerCase()));
    this.setState({ topMovies, popularMovies });
  }

  filterByRating(rating) {
    rating = (rating + 1) * 2;
    const topMovies = this.state.topMovies.filter((movie) => {
      if (movie.vote_average >= (rating - 2) && movie.vote_average <= rating) {
        return movie;
      }
    });
    const popularMovies = this.state.popularMovies.filter((movie) => {
      if (movie.vote_average >= (rating - 2) && movie.vote_average <= rating) {
        return movie;
      }
    });

    this.setState({ topMovies, popularMovies });
  }

  filterByNameAndRating(text, rating) {
    rating = rating * 2;
    const topMovies = this.state.topMovies.filter((movie) => {
      if (movie.title.toLowerCase().includes(text.toLowerCase()) && (movie.vote_count >= (rating - 2) && movie.vote_count <= rating)) {
        return movie;
      }
    });
    const popularMovies = this.state.popularMovies.filter((movie) => {
      if (movie.title.toLowerCase().includes(text.toLowerCase()) && (movie.vote_count >= (rating - 2) && movie.vote_count <= rating)) {
        return movie;
      }
    });



    this.setState({ topMovies, popularMovies });
  }

  activateRatingFilter(enabled, rating) {
    if (!enabled && this.currentFilteredText === "") {
      this.setState({ topMovies: this.topMoviesCopy, popularMovies: this.popularMoviesCopy });
      return;
    } else if (!enabled && this.currentFilteredText !== "") {
      this.filterByName(this.currentFilteredText);
      return;
    }
    if (this.currentFilteredText === "") {
      this.filterByRating(rating);
    } else {
      this.filterByNameAndRating(this.currentFilteredText, rating);
    }
  }

  goToMovieDetail(movieId) {
    let movie = this.state.popularMovies.find(movie => movie.id === movieId) || this.state.topMovies.find(movie => movie.id === movieId);
    this.selectedMovie = movie;
    this.setState({ isOnDetailView: true });
  }

  showMainPage() {
    this.selectedMovie = null;
    this.setState({ isOnDetailView: false });
  }


  render() {
    let isOnDetailView = this.state.isOnDetailView;

    if (isOnDetailView) {
      return (<MovieDetail goBack={this.showMainPage} movie={this.selectedMovie}></MovieDetail>);
    } else {
      return (
        <div className="movies-main-container">
          <Header
            handleSearchBoxTextChange={e => this.filterMovies(e.target.value)}
            handleStarSelected={this.activateRatingFilter}>
          </Header>
          <h1 className="movies-main-recommendations-title">Recommended by us</h1>
          <section>
            <MoviesList
              movies={this.state.topMovies}
              handleMovieSelected={this.goToMovieDetail}>
            </MoviesList>
          </section>
          <h1 className="movies-popular-title">More popular</h1>
          <section>
            <MoviesList
              movies={this.state.popularMovies}
              handleMovieSelected={this.goToMovieDetail}>
            </MoviesList>
          </section>
        </div>
      );
    }
  }
}

export default App;
