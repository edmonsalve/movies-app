
// URL's for the Movies API
export const API_BASE_URL = "https://api.themoviedb.org/3";
export const API_IMAGES_BASE_URL = "http://image.tmdb.org/t/p/";
export const API_POPULAR_MOVIES_URL = "/movie/popular";
export const API_TOP_MOVIES_URL = "/movie/top_rated";
export const API_KEY = "c92c5a09b322f5c8bd496a5a57c3dbab";