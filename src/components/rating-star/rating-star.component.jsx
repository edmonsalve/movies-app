import React from "react";

import "./rating-star.component.css";

export const RatingStar = props => (
    <span onClick={() => props.onStarClicked(props.id)} className={"icon-star " + (props.enabled ? "star-activated" : "star")}></span>
)