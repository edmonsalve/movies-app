import React, { Component } from "react";
import { SearchBar } from "../search-bar/search-bar.component";
import { RatingStar } from "../rating-star/rating-star.component";

import "./header.styles.css";

export class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            starFiltered: '',
            stars: [
                {
                    enabled: false,
                    id: 0,
                },
                {
                    enabled: false,
                    id: 1,
                },
                {
                    enabled: false,
                    id: 2,
                },
                {
                    enabled: false,
                    id: 3,
                },
                {
                    enabled: false,
                    id: 4,
                }
            ]
        }

        this.handleStarClicked = this.handleStarClicked.bind(this);
    }

    handleStarClicked(number) {
        let {stars} = this.state;
        let enabled = false;
        if (!stars[0].enabled) {
            for (let i = 0; i <= number; i++) {
                stars[i].enabled = true;
            }
            enabled = true;
        } else {
            for (let i = 0; i < stars.length; i++) {
                stars[i].enabled = false;
            }
            enabled = false;
        }
        this.setState({ stars }, () => {
            this.props.handleStarSelected(enabled, number)
        });
    }

    render() {
        return (
            <div className="movies-header">
                <SearchBar handleChange={this.props.handleSearchBoxTextChange}>
                </SearchBar>
                <span>Rating filter: </span>
                { this.state.stars.map(star => (
                    <RatingStar 
                        key={star.id}
                        id={star.id}
                        enabled={star.enabled}
                        onStarClicked={this.handleStarClicked}>
                    </RatingStar>
                ))}
            </div>
        )
    }
}