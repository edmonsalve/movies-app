import React from "react";

import "./search-bar.styles.css";

export const SearchBar = props => (
    <input
        className="movies-searchbar"
        placeholder="Find something..."
        onChange={props.handleChange}
    />
)