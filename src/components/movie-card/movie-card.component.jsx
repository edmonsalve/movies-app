import React from "react";
import { API_IMAGES_BASE_URL } from "../../utils/constants";

import './movie-card.styles.css';

export const MovieCard = props => {
    let imageWidth = "w185";
    return (
        <div className="movie-card" onClick={() => props.onMovieSelected(props.id)}>
            <img 
                alt="movie"
                src={`${API_IMAGES_BASE_URL}${imageWidth}/${props.imageUrl}`}
            />
            <h3>{props.title}</h3>
            <h5 className="ranking">Votes Rating: {props.rating}/10</h5>
        </div>
    )
}