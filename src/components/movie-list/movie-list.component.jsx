import React from 'react';
import { MovieCard } from "../movie-card/movie-card.component";

// third part dependencies
import { v4 as uuid } from "uuid";

import './movie-list.styles.css';

export const MoviesList = props => {
    if (!props.movies) return;
    return (
        <div className="movies-container">
            { props.movies.map(movie => (
                <MovieCard
                    key={uuid()}
                    id={movie.id}
                    imageUrl={movie.poster_path}
                    title={movie.title}
                    rating={movie.vote_average}
                    onMovieSelected={props.handleMovieSelected}>
                </MovieCard>
            ))}
            
        </div>
    )
};
