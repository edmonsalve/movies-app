import React from "react";

import "./movie-detail.styles.css";

import { API_IMAGES_BASE_URL } from "../../utils/constants";

export const MovieDetail = props => {
    let imageWidth = "w500";

    return (
        <div className="movie-detail-container">
            <section className="movie-detail-poster-container">
                <img alt="movie" src={`${API_IMAGES_BASE_URL}${imageWidth}/${props.movie.poster_path}`}/>
            </section>
            <section className="movie-detail-information">
                <div className="movie-preview">
                    <h1>{props.movie.title}</h1>
                    <h2>Overview:</h2>
                    <span>{props.movie.overview}</span>
                </div>
                <div className="movie-information-item">
                    <h3>Audience: {props.movie.adult? "Only for adults": "All"}</h3>
                </div>
                <div className="movie-information-item">
                    <h3>Release date: {props.movie.release_date}</h3>
                </div>
                <div className="movie-information-item">
                    <h3>Original Language and title: {(`${props.movie.original_language} - ${props.movie.original_title}`)}</h3>
                </div>
                <div className="movie-information-item">
                    <h3>Rating: {props.movie.vote_average}</h3>
                </div>
            </section>
            <div onClick={() => props.goBack()} className="movie-detail-go-back-button">{"<"}</div>
        </div>
    )
}